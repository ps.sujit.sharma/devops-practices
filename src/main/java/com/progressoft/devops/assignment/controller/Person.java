package com.progressoft.devops.assignment.controller;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.Scanner;

public class Person {

    private final Long id;

    @NotEmpty
    private final String name;

    @Min(1)
    private final int age;

    public Person(Long id, @NotEmpty String name, @Min(1) int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }


    public static long power(int base, int exponent) {
        long powResult = 1;
        for (int i = 0; i < exponent; i++) {
            powResult = powResult * base;
        }
        return powResult;
    } // function to take string and return the reverse

    public static String reverse(String s) {
        String result = "";
        for (int i = s.length() - 1; i >= 0; i--) {
            result += s.charAt(i);
        }
        return result;
    } // function to reverse Array and return the new Array reversed

    public static int[] arrReverse(int[] a) {
        int[] result = new int[a.length];
        int j = 0;
        for (int i = a.length - 1; i >= 0; i--) {
            result[j] = a[i];
            j++;
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); // take an integer array from user
        System.out.println("Enter your Array length : ");
        int len = input.nextInt();
        int[] arr = new int[len];
        System.out.println("Enter your numbers : ");
        for (int i = 0; i < len; i++) {
            System.out.println("The number " + i + " = ");
            arr[i] = input.nextInt();
        } // invoke Array reverse function
        int[] torev = arrReverse(arr);
        for (int i = 0; i < torev.length; i++) {
            System.out.println("the reverse Array " + i + " = " + torev[i]);
        } // the a problem here running the eclipse scape this step // invoke string reverse function
        System.out.println("Enter your string : ");
        String myString = input.nextLine();
        String Apply = reverse(myString);
        System.out.println("Your reverse string is : " + Apply); // invoke power function // (x)10 + (x)7 + (2x)5 + (10x)3
        System.out.println("Enter your x number: ");
        int x = input.nextInt();
        long v1 = power(x, 10);
        long v2 = power(x, 7);
        long v3 = power(2, 5); long v4 = power(10, 3); long result = v1 + v2 + v3 + v4;
        System.out.println("your input : " + x);
        System.out.print("your result : " + result);
    }
}
